from typing import Tuple

import click
import configparser

import os
from mmir.types import Matrix
from skimage.io import imread

from mmir.camera.camera import Camera, compute_fundamental_matrix
from mmir.mapping.mapping_visualizer import MappingVisualizer
from mmir.pipeline import Pipeline
from mmir.pipeline.interceptor import Interceptor
from mmir.stereo.disparity_map_calculator import DisparityMap
from mmir.stereo.disparity_transfer import MappingCalculator, OneToOneMapping
from mmir.stereo.sgbm_disparity_map_calculator import SGBMDisparityMapCalculator

BASE_PATH = os.path.dirname(os.path.realpath(__file__))
config_parser = configparser.ConfigParser()


def define_pipeline(interceptor: Interceptor, output: str, fuse: bool, with_disparity_map: bool = False) -> Pipeline:
    left_to_lwir, right_to_lwir = compute_fundamental_matrices()

    if not with_disparity_map:
        return Pipeline([
            SGBMDisparityMapCalculator(),
            MappingCalculator(left_to_lwir, right_to_lwir),
            interceptor,
            MappingVisualizer(fuse=fuse, output_file_path=output),
        ])

    return Pipeline([
        MappingCalculator(left_to_lwir, right_to_lwir),
        interceptor,
        MappingVisualizer(fuse=fuse, output_file_path=output),
    ])


def construct_input(left_image, right_image, disparity_map):
    left_image = imread(os.path.join(BASE_PATH, left_image))
    right_image = imread(os.path.join(BASE_PATH, right_image))

    if disparity_map is None:
        return left_image, right_image

    disparity_map = imread(os.path.join(BASE_PATH, disparity_map))
    return [DisparityMap(left_image, right_image, disparity_map)]


def compute_fundamental_matrices() -> Tuple[Matrix, Matrix]:
    left_int = config_parser['camera']['left_int']
    left_ext = config_parser['camera']['left_ext']
    right_int = config_parser['camera']['right_int']
    right_ext = config_parser['camera']['right_ext']
    lwir_int = config_parser['camera']['lwir_int']
    lwir_ext = config_parser['camera']['lwir_ext']

    lwir_camera = Camera.from_files(
        os.path.join(BASE_PATH, lwir_int),
        os.path.join(BASE_PATH, lwir_ext),
    )

    left_camera = Camera.from_files(
        os.path.join(BASE_PATH, left_int),
        os.path.join(BASE_PATH, left_ext),
    )

    right_camera = Camera.from_files(
        os.path.join(BASE_PATH, right_int),
        os.path.join(BASE_PATH, right_ext),
    )

    left_to_flir = compute_fundamental_matrix(left_camera, lwir_camera)
    right_to_flir = compute_fundamental_matrix(right_camera, lwir_camera)

    return left_to_flir, right_to_flir


def register(left_image, right_image, lwir_image, disparity_map, output, fuse):
    interceptor = Interceptor()

    pipeline = define_pipeline(interceptor, output, fuse, disparity_map is not None)

    lwir_image = imread(os.path.join(BASE_PATH, lwir_image))
    interceptor.intercept = lambda output: OneToOneMapping(output.left, lwir_image, output.mapping)
    pipeline.run(*construct_input(left_image, right_image, disparity_map))


@click.command()
@click.option('--fuse', default=False, is_flag=True)
@click.option('--config', default='config.ini', type=click.Path(exists=True, dir_okay=False))
@click.option('-l', '--left-image', prompt=True, type=click.Path(exists=True, dir_okay=False))
@click.option('-r', '--right-image', prompt=True, type=click.Path(exists=True, dir_okay=False))
@click.option('-lw', '--lwir-image', prompt=True, type=click.Path(exists=True, dir_okay=False))
@click.option('-d', '--disparity-map', type=click.Path(exists=True, dir_okay=False))
@click.option('-o', '--output', default='result.png')
def run(fuse, config, left_image, right_image, lwir_image, disparity_map, output):
    config_parser.read(config)
    register(left_image, right_image, lwir_image, disparity_map, output, fuse)


if __name__ == '__main__':
    run()
